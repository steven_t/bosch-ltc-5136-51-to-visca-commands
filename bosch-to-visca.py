# Codename: Cecylia02-2
import serial
import time

def convert_input(input_data):
    # Check if input starts with "87 00 00 08" and consists of 8 bytes
    if input_data[:4] == b"\x87\x00\x00\x08" and len(input_data) == 8:
        y_axis_force = input_data[4]  # Get Y axis force in hex
        x_axis_force = input_data[5]  # Get X axis force in hex
        direction = input_data[6]  # Get direction
        custom_b_byte = input_data[7]  # Get custom B byte
        
        # Convert x_axis_force from (1-7)(0/8) to (0-1)(0-4)
        x_axis_force_converted = (x_axis_force & 0x01) << 2 | (x_axis_force & 0x70) >> 4
        y_axis_force_converted = y_axis_force

        # Debug print statements
        print(f"1,2,3,4 - Address bytes: {' '.join(format(byte, '02x') for byte in input_data[:4])}")
        print(f"5 - Y-axis force: {format(y_axis_force, '02x')} (Converted: {format(y_axis_force_converted, '02x')})")
        print(f"6 - X-axis force: {format(x_axis_force, '02x')} (Converted: {format(x_axis_force_converted, '02x')})")
        print(f"7 - Direction: {format(direction, '02x')}")
        print(f"8 - Custom B byte: {format(custom_b_byte, '02x')}")

        mode_zoom = False
        mode_focus = False
# MAKE DIRECTION AS SUM, NOT AS A TABLE

        if direction == 0x01:
            direction_out = b"\x02\x03"  # right
        elif direction == 0x02:
            direction_out = b"\x01\x03"  # left
        elif direction == 0x00:
            if x_axis_force == 0x01:
                 mode_focus = True
            else:
                 direction_out = b"\x03\x03"  # no direction and no force == STOP
        elif direction == 0x08:
            direction_out = b"\x03\x01"  # up
        elif direction == 0x04:
            direction_out = b"\x03\x02"  # down

        elif direction == 0x09:
            direction_out = b"\x02\x01"  # UP RIGHT
        elif direction == 0x05:
            direction_out = b"\x02\x02"  # DOWN RIGHT
        elif direction == 0x06:
            direction_out = b"\x01\x02"  # DOWN LEFT
        elif direction == 0x0a:
            direction_out = b"\x01\x01"  # UP LEFT

        elif direction == 0x10:
            direction_out = b"\x31"  # twist_left
            mode_zoom = True
        elif direction == 0x20:
            direction_out = b"\x21"  # twist_right
            mode_zoom = True
        elif direction == 0x40:
            mode_focus = True
        else:
            print("Invalid direction")
            return None
#        print(f"direction out: {format(direction_out, '02x')}")
        
        # Prepare output bytes
        if mode_zoom == True:
              output_data = b"\x81\x01\x04\x07" + direction_out + b"\xFF" # Zoom in/out
        elif mode_focus == True:
              if direction == 0x40:
                   if x_axis_force == 0x01:
                        output_data = b"\x81\x01\x04\x18\x01\xFF" # One Push AF
                        print("One push Auto-Focus")
                        time.sleep(1)
                   else:
                        output_data = b"\x81\x01\x04\x08\x02\xFF"  # Fofcus Far
              else:
                   output_data = b"\x81\x01\x04\x08\x03\xFF" # Focus Near
        else:
              output_data = b"\x81\x01\x06\x01" + x_axis_force_converted.to_bytes(1, 'big') + y_axis_force_converted.to_bytes(1, 'big') + direction_out + b"\xFF" # Tilt somewhere

        # Debug print statement
        print(f"Output data: {' '.join(format(byte, '02x') for byte in output_data)}")

        return output_data
    else:
        print("Invalid input data")
        return None

def main():
    # Serial port settings
    input_port = "/dev/ttyUSB0"
    output_port = "/dev/ttyS0"
    baud_rate = 9600

    # Open serial ports
    with serial.Serial(input_port, baud_rate) as input_ser, \
         serial.Serial(output_port, baud_rate) as output_ser:

        while True:
            # Read input from /dev/ttyUSB0
            input_data = input_ser.read(8)

            # Check if received bytes start with "87 00 00 08" and consist of 8 bytes
            if input_data[:4] == b"\x87\x00\x00\x08" and len(input_data) == 8:
                # Print input
                print(f"Received: {' '.join(format(byte, '02x') for byte in input_data)}")

                # Convert input
                output_data = convert_input(input_data)

                # Send output to /dev/ttyS0
                if output_data:
                    output_ser.write(output_data)
                    print(f"Sent: {' '.join(format(byte, '02x') for byte in output_data)}")

                    # Read and print output from /dev/ttyS0
#                    output_response = output_ser.read(8)
#                    print(f"Received from /dev/ttyS0: {' '.join(format(byte, '02x') for byte in output_response)}")
            else:
                print("Invalid input length or start sequence")

if __name__ == "__main__":
    main()

